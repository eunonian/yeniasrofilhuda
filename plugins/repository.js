import createRepository from '../repositories'

export default (ctx, inject) => {
  inject('repo', createRepository(ctx.$axios))
}
