// plugins/vuex-orm-axios.js

import { Model } from '@vuex-orm/core'

export default ({ $axios }) => {
  $axios.onRequest((config) => {
    console.log('Making request to ' + config.url)
  })
  $axios.onResponse((response) => {
    return response.data
  })
  Model.setAxios($axios)
}
