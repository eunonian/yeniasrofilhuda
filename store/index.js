import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import VuexORMAxios from '@vuex-orm/plugin-axios'
import database from '@/store/database'

VuexORM.use(VuexORMAxios)
export const plugins = [
  VuexORM.install(database)
]
