import { Database } from '@vuex-orm/core'

import SchoolStore from '@/store/models/school-store'

const database = new Database()

database.register(SchoolStore)
export default database
