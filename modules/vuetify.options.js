// import colors from 'vuetify/es5/util/colors'

export default {
  treeShake: true,
  defaultAssets: {
    icons: 'md',
    font: {
      family: 'Quicksand'
    }
  },
  customVariables: ['~/assets/variables.scss'],
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#00B4D8',
        accent: '#FA8E40',
        secondary: '#828282',
        info: '#00BCD4',
        warning: '#FF9800',
        error: '#FF4433',
        success: '#4CAF50'
        // ORIGIN DARK COLORS
        // primary: colors.blue.darken2,
        // accent: colors.grey.darken3,
        // secondary: colors.amber.darken3,
        // info: colors.teal.lighten1,
        // warning: colors.amber.base,
        // error: colors.deepOrange.accent4,
        // success: colors.green.accent3
      }
    }
  }
}
