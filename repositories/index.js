import SchoolRepository from './school-repository'

export default (http) => {
  return {
    school: SchoolRepository(http)
  }
}
